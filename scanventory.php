<?php
/*
Plugin Name: Scanventory
Description: Manage and administrate inventory with a suite of inventory control software.
Version: 1.1.1
Author : Scanventory.com

Copyright 2013 Scanventory All Rights Reserved
*/
global $wp_version;
if (!defined('PHP_VERSION_ID')) {
	$version = explode('.', PHP_VERSION);
	define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}
if (PHP_VERSION_ID < 50102) {
	die('Scanventory requires PHP Version 5.1.2 or greater.');
}
$exit_msg_ver = 'Sorry, this plugin is not supported on pre-3.1 WordPress installs.';
$exit_msg_woo = 'Sorry, this plugin requires Woocommerce.';

if (version_compare($wp_version,"3.1","<")) { exit($exit_msg_ver); }

define("SCANVENTORY_VERSION","1.02");

define("SCANVENTORY_DIR", plugin_dir_path(__FILE__));
define("SCANVENTORY_ASSETS_DIR", SCANVENTORY_DIR."assets/");
define("SCANVENTORY_CLASSES_DIR", SCANVENTORY_DIR."classes/");
define("SCANVENTORY_TEMPLATES_DIR", SCANVENTORY_DIR."templates/");

spl_autoload_register(function ($className) {
    if (substr($className, 0, strlen("scanventory")) === "scanventory") {
        $classNameShort = str_replace("\\", "/", substr($className, strlen("scanventory\\")));
		$file = SCANVENTORY_CLASSES_DIR . $classNameShort.".php";
		if (file_exists($file)) {
			include_once $file;
		}
    }
});

$key = scanventory_license::instance();
$scanventory = scanventory_control::instance( $key );

if ( is_admin() ) {
	$scanventoryAdmin = scanventory_admin::instance( $key );

	add_action( 'admin_init', array($scanventoryAdmin, 'register'));
	add_action( 'admin_menu', array($scanventoryAdmin,'menu') );
	add_action('load-edit.php', array($scanventoryAdmin,'custom') );
	add_action('admin_footer-edit.php',	array($scanventoryAdmin,'check'));
}

?>