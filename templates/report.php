<?php
if (!is_user_logged_in() || !current_user_can('manage_options')) wp_die('This page is private.');
?>
<style>

.report-section table {
	width:100%;
}
.report-section table tr:nth-child(even) {background: #DDD}
.report-section table tr:nth-child(odd) {background: #FFF}

.report-section table thead {
	font-weight:bold;
	background-color: #cdcdcd;
	margin:0px;
	padding:5px;
}

</style>
<section class='report-section'>
	<?php
	global $woocommerce;
	?>
	<BR>
	<h2>Simple</h2>
	<table cellspacing="0" cellpadding="2">
		<thead>
		<tr>
			<th scope="col" style="text-align:left;"><?php _e('SKU', 'woothemes'); ?></th>
			<th scope="col" style="text-align:left;"><?php _e('Stock', 'woothemes'); ?></th>
			<th scope="col" style="text-align:left;"><?php _e('Product', 'woothemes'); ?></th>
			<th scope="col" style="text-align:left;"><?php _e('Price / Regular', 'woothemes'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php

		$args = array(
			'post_type'	=> 'product',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'orderby'	=> 'title',
			'order'	=> 'ASC',
			'meta_query' => array(
				array(
					'key' => '_manage_stock',
					'value' => 'yes'
				)
			),
			'tax_query' => array(
				array(
					'taxonomy' => 'product_type',
					'field' => 'slug',
					'terms' => array('simple'),
					'operator' => 'IN'
				)
			)
		);
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();
			$product = new WC_Product( get_the_ID() );
			$price = (($product->price != "") ? money_format('%i',$product->price) : '0.00');
			$regular = (($product->regular_price != "") ? money_format('%i',$product->regular_price) : '0.00');
			$sku = (($product->sku != "") ? $product->sku : '<i>None</i>');
			?>
			<tr>
				<td><?php echo $sku; ?></td>
				<td><?php echo $product->stock; ?></td>
				<td><?php echo $product->get_title(); ?></td>
				<td><?php echo "{$price} / {$regular}"; ?></td>
			</tr>
		<?php
		endwhile;
		?>
		</tbody>
	</table>
	<h2>Variations</h2>
	<table cellspacing="0" cellpadding="2">
		<thead>
		<tr>
			<th scope="col" style="text-align:left;"><?php _e('SKU', 'woothemes'); ?></th>
			<th scope="col" style="text-align:left;"><?php _e('Stock', 'woothemes'); ?></th>
			<th scope="col" style="text-align:left;"><?php _e('Variation', 'woothemes'); ?></th>
			<th scope="col" style="text-align:left;"><?php _e('Parent', 'woothemes'); ?></th>
			<th scope="col" style="text-align:left;"><?php _e('Attributes', 'woothemes'); ?></th>
			<th scope="col" style="text-align:left;"><?php _e('Price / Regular', 'woothemes'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$args = array(
			'post_type'	=> 'product_variation',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'orderby'	=> 'title',
			'order'	=> 'ASC',
			'meta_query' => array(
				array(
					'key' => '_stock',
					'value' => array('', false, null),
					'compare' => 'NOT IN'
				)
			)
		);
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();
			$product = new WC_Product_Variation( $loop->post->ID );
			$attrs = array();
			if ($product->variation_data != "") {
				$terms = $woocommerce->get_attribute_taxonomies();

				foreach ( $terms as $term) {
					$termMap['attribute_pa_'.$term->attribute_name] = $term->attribute_label;
				}

				foreach ($product->variation_data as $attributeKey=>$value) {
					if (isset($termMap[$attributeKey])) {
						$attrs[] = $termMap[$attributeKey]." : ".$value;
					} else {
						$attrs[] = $value;
					}
				}

				$price = (($product->price != "") ? money_format('%i',$product->price) : '0.00');
				$regular = (($product->regular_price != "") ? money_format('%i',$product->regular_price) : '0.00');
				$sku = (($product->sku != "") ? $product->sku : '<i>None</i>');
			}

			?>
			<tr>
				<td><?php echo $sku; ?></td>
				<td><?php echo $product->stock; ?></td>
				<td><?php echo $product->get_title(); ?></td>
				<td><?php echo get_the_title( $loop->post->post_parent ); ?></td>
				<td><?php echo join(", ",$attrs)?></td>
				<td><?php echo $price;?> / <? echo $regular; ?></td>
			</tr>
		<?php
		endwhile;
		?>
		</tbody>
	</table>
</section>