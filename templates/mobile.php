<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=480" />
	<link rel="stylesheet" type="text/css" href="<?= plugins_url('assets/css/bootstrap.css',__DIR__) ?>" />
	<link rel="stylesheet" type="text/css" href="<?= plugins_url('assets/css/mobile.css',__DIR__) ?>" />
	<link rel="stylesheet" type="text/css" href="<?= plugins_url('assets/css/bootstrap-theme.css',__DIR__) ?>" />
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
</head>
<body>

<? if ($action == 'login') { ?>

<form class="form-inline" role="form" method="POST" action="<?= $mobile_url ?>">
	<div class="panel panel-primary">

		<div class="panel-heading">Scanventory &mdash; Login</div>

		<div class="panel-body">
			<label class="sr-only" for="username">Username</label>
			<input type="username" name="username" class="form-control" placeholder="Username">
		</div>

		<div class="panel-body">
			<label class="sr-only" for="pass">Password</label>
			<input type="password" name="password" class="form-control" placeholder="Password">
		</div>
		<div class="panel-body">
			<button type="submit" class="btn btn-primary pull-right">Sign in</button>
		</div>
	</div>

	<? if ($message) { echo "<div class=\"panel-body\">"; echo "<div class=\"alert alert-warning\"> {$message} </div>"; echo "</div>"; } ?>

</form>

<? } elseif ($action =='view') { ?>

<form method="POST" action="<?= $mobile_url ?>">
	<div class="nav navbar navbar-fixed-top">
		<div class="col-xs-9 pull-left">
			<i class="icon-qrcode"></i> Scanventory
		</div>
		<div class="col-xs-3 pull-right text-right">
			<A class="btn" HREF="<?=$mobile_url?>&logout=1">Logout</A> &nbsp; &nbsp;
		</div>
	</div>

	<div class="container" id="main" style="text-align:center">

		<div class="row">
			<div class="col-xs-6">
				<?=$image;?>
				<p class="sku">SKU: <?=$sku;?></p>
			</div>

			<div class="col-xs-6">
				<p class="name"><?=$name;?></p>
				<? if ($stock === FALSE) { ?>
					Stock not managed <BR>
					<button type='submit' class='btn btn-primary' name='cmd' value='Activate'><i class='icon-off'></i> Activate</button>
				 <? } else { ?>
				<p class="name"><div id="stock"><?=$stock;?></div></p>
				<? if ($instock == "instock") { echo "<button type='submit' name='cmd' value='instock' class='btn btn-success btn-xs'>In stock</button>"; } else { echo "<button type='submit' name='cmd' value='outstock' class='btn btn-warning btn-xs'>Out of stock</button>"; } } ?>

			</div>
		</div>

		<div class="col text-center">
			<?= $message; ?>
			<p class="name" id="message"></p>
		</div>
		<?php
		if ($stock !== FALSE ) {
		?>
				<div class='row main'>
					<P><B>Main Inventory</B></P>
					<div class='col-xs-6 text-center'>
						<div class="input-group">
							<input type="number" class="form-control stock-input" name="main_stock" value="<?=$stock?>">
                            &nbsp;
							<div class='btn-group'>
                                <button type="submit" name="main_byone" value="&#45;" class="btn btn-xs btn-primary" <?= ($stock > 0 ? '' : 'disabled') ?>><i class='icon-minus'></i></button>
								<button class="btn btn-default btn-xs" type="submit" name="main_set">Set</button>
                                <button type="submit" name="main_byone" value="&#43;" class="btn btn-xs btn-primary" ><i class='icon-plus'></i></button>
							</div>
						</div>
					</div>
					<div class='col-xs-6 text-center'>


							<div class='input-group'>
								<label>Allow backorders
								<SELECT name="backorders">
									<OPTION value="yes" <?= ($backorders == "yes"?"selected":"")?>>Yes</OPTION>
									<OPTION value="notify" <?= ($backorders == "notify"?"selected":"")?>>Yes, notify customers</OPTION>
									<OPTION value="no" <?= ($backorders == "no"?"selected":"")?>>No</OPTION>
								</SELECT>
								</label>
							</div>
							<div class='input-group'>
								<label>Sell individually
									<SELECT name="individually">
										<OPTION value="yes" <?= ($individually == "yes"?"selected":"")?>>Yes</OPTION>
										<OPTION value="no" <?= ($individually == "no"?"selected":"")?>>No</OPTION>
									</SELECT>
								</label>
							</div>
							<div class='btn-group'>
								<input type="submit" name="cmd" value="Update" class="btn btn-primary">
							</div>

						</div>
					</div>
				</div>
			<?php
			if ((count($vars) > 0) and ($stock !== FALSE)) {
				foreach($vars as $var) {
				?>
					<div class='row variant'>
						<P><B><?= $var["label"]; ?></B></P>
						<div class='col-xs-6 text-center'>
                            <?php if ($var["has_stock"]) { ?>
							<div class="input-group">
								<input type="number" class="form-control stock-input" name="stock[<?=$var['vid']?>]" value="<?=$var['stock']?>">
                                &nbsp;
                                <div class='btn-group'>
									<button type="submit" name="byone[<?=$var["vid"]?>]" value="&#45;" class="btn btn-xs btn-primary" <?= ($var['stock'] > 0 ? '' : 'disabled') ?>><i class='icon-minus'></i></button>
									<button class="btn btn-default btn-xs" type="submit" name="set[<?=$var["vid"]?>]">Set</button>
									<button type="submit" name="byone[<?=$var["vid"]?>]" value="&#43;" class="btn btn-xs btn-primary" ><i class='icon-plus'></i></button>
								</div>
							</div>
                            <?php } ?>
						</div>
						<div class="col-xs-6 text-left">
							<P><B>Attributes : </B></P>
							<ul class='attributes'>
								<?php
									if (count($var["attributes"]) > 0) {
										foreach ($var["attributes"] as $attributeKey=>$value) {
											list($akey,$avalue) = $value;
											echo "<li><B>".$akey."</B> : ".$avalue."</li>";
										}
									}
								?>
							</ul>
						</div>
					</div>
				<?php
				}
			}
			?>
			<div class="col-xs-12">
				<button type='submit' name='cmd' value='Deactivate' id='deactButton' class='btn btn-default pull-right'> <i class='icon-off'></i> Deactivate </button>
			</div>
			<?php
		}
	}
?>
		<BR>
	</div>
</form>
</body>
</html>