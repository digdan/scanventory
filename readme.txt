=== Scanventory  ===
Contributors: Scanventory
Tags: woocommerce, scan, inventory, inventory management, stock management, stock control, inventory control
Requires at least: 3.0.1
Tested up to: 3.8.1
Stable tag: 1.0.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Quick mobile inventory scanning and management solution

== Description ==

Scanventory is an innovative suite of tools to help manage and maintain inventory control of your WooCommerce store. Free yourself from the toils of inventory control with Scanventory, the scannable, mobile, inventory manager for your WooCommerce store.

**Features include**

* Mobile inventory control
* Detailed stock reports
* Custom label design and printing
* Detailed stock reports
* Restock cancelled/refunded orders
* Inventory Control Audit Trail

== Installation ==

It is recommended that you install the WooCommerce Inventory Managment automatically, through the WordPress.org plugin directory:

1. Log into your WordPress admin area and click the 'Plugins' menu item in WordPress.
1. Click 'Add New' at the top of the page.
1. Enter "scanventory" in the search box and click the 'Search Plugins' button.
1. Look for "WooCommerce Inventory Management" by Scanventory and click the "Install Now" button.
1. Once installed, activate the plugin and then follow our installation instructions to complete the setup: http://www.scanventory.net.

If you are unable to use the plugin directory, follow these steps:

1. Download the latest version of the plugin from here, but leave it as a zip file.
1. Log into your WordPress admin area and click the 'Plugins' menu item in WordPress.
1. Click 'Add New' at the top of the page.
1. Click the 'Upload' link at the top of the page.
1. Click the 'Choose File' button and select the plugin zip file you saved in step 1.
1. Click the 'Install Now' button and wait for WordPress to finish the process.
1. Click the 'Activate Plugin' link after WordPress displays 'Plugin installed successfully'.
1. Follow our installation instructions to complete the setup: http://www.scanventory.net.


== Frequently Asked Questions ==

**How does it work?**

* Scanventory is a plugin that is used alongside with Woocommerce, which is a WordPress E-Commerce solution.
* The Scanventory system allows the user to print product labels that have a custom QR code. It also allows the user to place labels on any product.
* The software uses the attached QR codes from your inventory to allow you to change product information such as the inventory stock, status and availability. Users can do this with a simple QR code scanner.
* The system can scan any label that is configured for a QR code reader/scanner app.

**Where can I get more information?**

You can find more information on the Scanventory plugin website http://www.scanventory.net.

== Screenshots ==


== Changelog ==

= 1.1.1 - 10/23/14 =
* Updated Reporting section

= 1.0.6 - 10/21/14 =
* Licensing allows multiple product types.
* Better error handling


= 1.0.5 - 5/5/14 =
* Cached licensing validation attempts to improve performance
* Checks PHP version for compatibility

= 1.0.4 - 3/26/14 =
* Added ability to allow backorders or sell products individually from mobile inventory manager
* Fixed logout bugs

= 1.0.3 - 03/04/2014 =
* Modified label layouts to fit longer names

= 1.0.2 - 02/27/2014 =
* Added support for trial & demonstrations.
