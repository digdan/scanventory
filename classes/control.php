<?php
class scanventory_control {

    private $key;

	CONST SV_SL_AUTHOR = 'Scanventory';
	CONST SV_SL_ITEM_NAME = 'Scanventory 6 month license';
	CONST SV_SL_STORE_URL = 'http://www.scanventory.net';

	private static $instance;

    public static function instance( scanventory_license $kv = NULL ) {
        if ( ! isset( self::$instance )) {
            self::$instance = new scanventory_control( $kv );
        }
        return self::$instance;
    }

	public function __construct( scanventory_license $kv = NULL ) {
		if ( ! is_null($kv)) {
            $this->key = $kv;
		}
        $this->init();
	}

	function init() {
        add_action('wp_loaded', array($this,'check'));
        add_filter('woocommerce_reduce_order_stock_quantity', array($this,'reduce_stock', 15,2));
    }

	function qr($txt) {
		include_once( SCANVENTORY_CLASSES_DIR . 'qr.php');
		status_header(200);
		nocache_headers();
		header("Content-type: image/png");
		echo QRcode::png( $txt );
		die();
	}

	function check() { //Init on start
		global $wp;
		$current= parse_url($_SERVER['REQUEST_URI']);

		if (strstr($current['query'],'scanventory-qr')) {
			$this->qr( 'http://'.urldecode($_REQUEST["q"]) );
		}

		if ($current['query'] == 'scanventory-labels') {
			$this->labels();
		}

		if ($current['query'] == 'scanventory-report') {
			$this->report();
		}

		if (strstr($current['query'],'scanventory-S')) { //Mobile
			$mobile = scanventory_mobile::instance( $this->key );
			if ($mobile->valid_request( $current['query'] ) ) {
				$mobile->display();
			} else {
				die('Invalid request.');
			}

		}

		add_action( 'woocommerce_order_status_processing_to_cancelled', array( $this, 'restore_order_stock' ), 10, 1 );
		add_action( 'woocommerce_order_status_completed_to_cancelled', array( $this, 'restore_order_stock' ), 10, 1 );
		add_action( 'woocommerce_order_status_on-hold_to_cancelled', array( $this, 'restore_order_stock' ), 10, 1 );
		add_action( 'woocommerce_order_status_processing_to_refunded', array( $this, 'restore_order_stock' ), 10, 1 );
		add_action( 'woocommerce_order_status_completed_to_refunded', array( $this, 'restore_order_stock' ), 10, 1 );
		add_action( 'woocommerce_order_status_on-hold_to_refunded', array( $this, 'restore_order_stock' ), 10, 1 );

	}

	public function restore_order_stock( $order_id ) {
		$restoreStock = get_option('scanventory_restoreStock');

		$order = new WC_Order( $order_id );

		if ( (! get_option('woocommerce_manage_stock') == 'yes') or (! sizeof( $order->get_items() ) > 0) or ($restoreStock === FALSE)) {
			return;
		}

		foreach ( $order->get_items() as $item ) {

			if ( $item['product_id'] > 0 ) {
				$_product = $order->get_product_from_item( $item );

				if ( $_product && $_product->exists() && $_product->managing_stock() ) {

					$old_stock = $_product->stock;

					$qty = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $this, $item );

					$new_quantity = $_product->increase_stock( $qty );

					$order->add_order_note( sprintf( __( 'Item #%s stock incremented from %s to %s.', 'woocommerce' ), $item['product_id'], $old_stock, $new_quantity) );

					$order->send_stock_notifications( $_product, $new_quantity, $item['qty'] );

					scanventory_log::instance("{".$item['product_id']."} stock restored from {$old_stock} to {$new_quantity} due to reversed order.");
				}
			}
		}
	}

	function reduce_stock($stock,$item) {
		scanventory_log::instance("{".$item."} stock decreased to {$stock} via order.");
	}

	function report() {
		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		status_header(200);
		nocache_headers();
		include_once( SCANVENTORY_TEMPLATES_DIR . 'report.php');
	}

	function shorten($in='',$length=15) {
		if (strlen($in) > $length) {
			$out = substr($in,0,($length-3))."...";
			return $out;
		} else {
			return $in;
		}
	}

	function labels() {
		global $wpdb,$woocommerce;


		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		status_header(200);
		nocache_headers();

		if (isset($_POST["products"])) {
			$psql = join(",",$_POST["products"]);
			$csql = (isset($_POST['variationed']) ? " OR p.post_parent IN ({$psql})" : "");
			$query = "SELECT DISTINCT p.id FROM {$wpdb->posts} p WHERE p.ID IN ({$psql}) {$csql} AND p.post_type = 'product'";
		} else {
			if ( ! isset($_POST['variationed'])) {
				$csql = "";
			}
			$csql = (isset($_POST['variationed']) ? "" : " AND p.post_parent='0'");
			$query = "SELECT DISTINCT p.id , date_format(p.post_date,'%Y') as sort_year, date_format(p.post_date,'%m') as sort_month FROM {$wpdb->posts} p,	{$wpdb->term_relationships} tr,	{$wpdb->term_taxonomy} tt WHERE	p.post_type = 'product' {$csql}";
		}


		$results = $wpdb->get_results( $query );

		$labels = array();

		$counter = 0;
		foreach($results as $pr) {
			$buffer = array();
			$en = sha1( $pr->id + get_option('scanventory_secret') );
			$ch = (substr( $en, (strlen($en) - 4) , 4 ));
			$buffer['code'] = dechex($pr->id)."g".$ch;
			if ( function_exists( 'get_product' ) )
				$p = get_product( $pr->id );
			else
				$p = new WC_Product( $pr->id );

			if (substr($_POST['variable'],0,1) == 'a') {
				//Variable is attribute
				$vp = explode(":",$_POST['variable']);
				$d = woocommerce_get_product_terms($p->id,$vp[1],'ids');
				$buffer['variable'] = array_shift(woocommerce_get_product_terms($p->id, $vp[1], 'names'));
			} elseif ($_POST['variable'] == 'price') {
				$buffer['variable'] = $p->get_price_html();
			} elseif ($_POST['variable'] == '*') {
				$terms = $woocommerce->get_attribute_taxonomies();
				$buffer['variable'] = '';
				$varMap = array();
				foreach($terms as $k=>$term) {
					$varMap[$term->attribute_label] = woocommerce_get_product_terms($pr->id, 'pa_'.$term->attribute_name, 'names');
				}
				unset($varMap['']);
				foreach($varMap as $k=>$v) {
					if (count($v) > 0) {
						$buffer['variable'] .= "<B>{$k}</B> : ".join(", ",$v)."<BR>\n";
					}
				}
			}

			if ($p === FALSE) {
				echo 'Unable to locate product.';
				die();
			}

			$buffer['sku'] = $p->get_sku();
			$buffer['price'] = $p->get_price();
			$buffer['name'] = $this->shorten( $p->post->post_title , 25 );
			$buffer['image'] = get_the_post_thumbnail( $pr->id, array(200,200));
			$labels[$counter++] = $buffer;
			$short[$counter] = $buffer['code'];
		}

		//Fetch shortURLs in batch
		foreach($short as $k=>$code) {
			$conv[$code] = get_bloginfo('url').'/?scanventory-S'.$code;
		}

		$conv['s'] = get_bloginfo('url');
		$conv['l'] = $this->key->lkey();
		$conv['n'] = self::SV_SL_ITEM_NAME;

        //use IW.LC short service
		$url = "http://iw.lc/batch";
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$conv);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		$screturn = json_decode($response);

        if (isset($screturn->error)) {
            die($screturn->error);
        }

		$shortcodes = array();
		if ($screturn === false ) {
			die('Unable to generate labels. SS Error.');
		} else {
			foreach($screturn as $sk=>$sv) {
				$shortcodes[$sk] = $sv;
			}
		}

		//Include stylesheet
		echo "<html><head>";
		$thermal = false;

		//Find items per page :
		$mark = $_REQUEST["label"];
		$markp = explode("-",$mark);
		array_pop($markp);
		$perPage = array_pop($markp);

		if (strstr($_REQUEST['label'], 'thermal')) $thermal = true;
		echo "<link rel='stylesheet' href='".plugin_dir_url( SCANVENTORY_ASSETS_DIR."css/label/" ).'label/'.$_REQUEST["label"].".css' type='text/css' media='all' />";
		echo "</head><body>";

		echo "<div class='label-wrap'>";
		$count = 1;
		foreach($labels as $label) {
			$variable = $label['variable'];
			$short = urlencode($shortcodes[$label['code']]);
			echo "<div class='label'>";
			echo "<div class='qrcode'>";
            if ($short == 'over_limit') {
                echo "<img class='qrcode' src=\"http://placehold.it/89/ffffff&text=Over+Limit\">";
            } else {
			    echo "<img class='qrcode' src=\"/?scanventory-qr&q={$short}\">";
            }
			echo "</div>";
			echo "<div class='name'>";
			echo $label['name'];
			echo "</div>";
			echo "<div class='sku'>";
			echo $label['sku'];
			echo "</div>";
			echo "<div class='image'>";
			echo $label['image'];
			echo "</div>";
			echo "<div class='variable'>";
			echo $variable;
			echo "</div>";
			echo "</div>\n\n";
			//if ($thermal == true) echo "<div class='page-break'></div>";
			if ($count++ == $perPage) {
				echo "<div class='page-break'></div>\n\n";
				$count = 1;
			}
		}
		echo "</div>";//label-wrap
		echo "</body></html>";
		die();
	}
}
?>
