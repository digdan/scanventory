<?php

class scanventory_admin {

	private $options;

	private $key;

	private static $instance;

	public function __construct( scanventory_license $kv ) {
		$this->key = $kv;
	}

	public static function instance( scanventory_license $kv ) {
		if ( ! isset( self::$instance )) {
			self::$instance = new scanventory_admin( $kv );
		}
		return self::$instance;
	}

	// LABEL DEFINITIONS
	function labelfac() {
		return array(
			'2-sheet' => '8 1/2" x 5 1/2", 2 per Sheet ( Avery ®: 5126 / 5526 / 8126 )',
			'6-sheet' => '3 1/3" x 4", 6 per Sheet ( Avery ®: 5164 / 5264 / 8164 )',
			'2x4-10-sheet' => '2" x 4", 10 per Sheet ( Avery ®: 5163 / 5263 / 5963 / 8163 / 8463 )',
			'12-sheet' => '3.75" x 1.25", 12 per Sheet ( Avery ®: 6879 )'
		);
	}


	static function genlog() {
		$g = get_option('scanventory_log');
		if ($g==false) {
			$tn = time();
			$log = array(
				array($tn, get_current_user_id() , 'Scanventory installed.')
			);
			update_option('scanventory_log',$log);
			$g = $log;
		}
		return $log;
	}

	static function log($event) {
		$g = self::genlog();
		$tn = time();
		$g[] = array( $tn, get_current_user_id() , $event );
		if (count($g) > 250 ) {
			array_pop($g);
		}
		update_option('scanventory_log',$g);
	}

	static function gensecret() {
		$g = get_option('scanventory_secret');
		if ($g===false) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$randstring = '';
			for ($i = 0; $i < 10; $i++) {
				$randstring .= substr($characters,rand(0,strlen($characters)),1);

			}
			update_option('scanventory_secret',$randstring);
			$g = $randstring;
		}
		return $g;
	}

	function report() {
		include_once( SCANVENTORY_TEMPLATES_DIR . 'report.php');
		die();
	}

	function support() {
		global $form_message;
		$form_message = "";
		$admin_email = get_option('admin_email');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$sig = "\n\n--\nVersion : ". SCANVENTORY_VERSION ."\n";
			$sig .= "From : ".$admin_email."\n";
			$sig .= "LKey : ".$this->key->last()."\n";
			$sig .= "From : ".$_SERVER["REMOTE_ADDR"]."\n";
			$sig .= "@ ".date('r')."\n";
			$sig .= "Host : ".$_SERVER["HTTP_HOST"]."\n";
			$subject = "";
			if ($_POST["category"]) $subject .= ": ".$_POST["category"];

			$headers = "From: {$admin_email}\r\nReply-To: {$admin_email}\r\nX-Mailer: PHP/".phpversion();

			$didmail = wp_mail("info@scanventory.net","Support Request {$subject}", $_POST["message"].$sig, $headers);
			if ($didmail) {
				$form_message = "Support request sent.";
			} else {
				$form_message = "There was a problem sending the support request.";
			}
		}
		include_once( SCANVENTORY_TEMPLATES_DIR . 'support.php');
	}

	function main() {
		global $scanventory;
		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		if (isset($_REQUEST["labels"])) {
			$this->allcodes();
			die();
		}

		wp_enqueue_style('scanventory-style', plugin_dir_url( __DIR__ ).'assets/css/custom.css');

		if (($_SERVER["REQUEST_METHOD"] == "POST") and (isset($_POST["lkey"]))) { //Using key

			delete_transient("scanventory_keyvalid"); // Delete cache
			$pre = scanventory_license::instance()->keyvalid( $_POST["lkey"] );
			list ($preValid,$mess) = $pre;

			if ( ! $preValid) { //Not found, lets try to activate it.
				$api_params = array(
					'edd_action' => 'activate_license',
					'license' => $_POST["lkey"],
					'item_name' => urlencode( scanventory_control::SV_SL_ITEM_NAME )
				);
				$aurl = add_query_arg( $api_params, scanventory_control::SV_SL_STORE_URL );
				$activate = wp_remote_get( $aurl , array( 'timeout' => 15, 'sslverify' => false ) );

				$d = wp_remote_retrieve_body($activate);
				if ( is_wp_error( $activate ) )
					return false;

				$ret = json_decode( wp_remote_retrieve_body( $activate ) );
				if (is_object($ret)) {
					if (isset($ret->license) and ($ret->license=="valid")) {
						$this->options["lkey"] = $_POST["lkey"];
						update_option('scanventory_options',$this->options);
						$this->lic_valid = true;
						$this->lic_message = '';
					} else {
						$this->lic_message = "Unable to activate invalid key : {$_POST["lkey"]}";
						$this->lic_valid = false;
					}
				} else {
					$this->lic_message = "Unable to activate key.";
					$this->lic_valid = false;
				}
			}
		}

		$tabActive = ( isset($_GET["tab"]) ? $_GET["tab"] : 'main');

		?>
		<div class="wrap">
			<?php screen_icon(); ?>
			<h2>Scanventory</h2>
			<?php settings_errors(); ?>
			<h2 class="nav-tab-wrapper">
				<a href="?post_type=product&page=scanventory&tab=main" class="nav-tab<?=($tabActive=="main" ? ' nav-tab-active' : '' )?>">Main</a>
				<a href="?post_type=product&page=scanventory&tab=labels" class="nav-tab<?=($tabActive=="labels" ? ' nav-tab-active' : '' )?>">Labels</a>
				<a href="?post_type=product&page=scanventory&tab=log" class="nav-tab<?=($tabActive=="log" ? ' nav-tab-active' : '' )?>">Log</a>
				<a href="?post_type=product&page=scanventory&tab=report" class="nav-tab<?=($tabActive=="report" ? ' nav-tab-active' : '' )?>">Reports</a>
                <a href="?post_type=product&page=scanventory&tab=support" class="nav-tab<?=($tabActive=="support" ? ' nav-tab-active' : '' )?>">Support</a>
				<? if ( ! $this->key->valid()) { ?>
					<a href="http://www.scanventory.net" TARGET="_BLANK" class="nav-tab<?=($tabActive=="purchase" ? ' nav-tab-active' : '' )?>">Purchase</a>
				<? } ?>
			</h2>

<?
			if ($this->lic_message) {
				echo "<div id=\"message\" class=\"error fade\"><p><strong>".$this->lic_message."</strong></p></div>";
			}

			if ($tabActive == "main") {
				$this->mainDisplay();
			} elseif ($tabActive == "log") {
				$this->logDisplay();
			} elseif ($tabActive == "labels") {
				$this->labels();
			} elseif ($tabActive == "report") {
				$this->report();
			} elseif ($tabActive == "support") {
				$this->support();
			}
			?>
		</div>
	<?php
	}

	function mainDisplay() {
		$restoreStock = (isset($this->options['restoreStock']) ? true : false);
		$lkey = (isset($this->options['lkey']) ? $this->options['lkey'] : '');
		$lkey = (isset($_POST['lkey']) ? $_POST['lkey'] : $lkey );
	?>
		<H2>Settings</H2>
			<form method="post" action="?post_type=product&page=scanventory">
				<table class="form-table">
					<tr valign="top">
						<th scope="row">License Key</th>
						<td>
							<label for="lkey"><input style="width:250px" name="lkey" id="lkey" value="<?=$lkey;?>" type="text"></label>
						</td>
					</tr>
                    <tr valign="top">
                        <th scope="row">License Status</th>
                        <td>
                            <strong <?= ($this->key->valid() ? "style='color:green'":"style='color:red'")?>><?= $this->key->status()?></strong>
                        </td>
                    </tr>
					<? if ($this->key->valid() === TRUE) { ?>
					<tr valign="top">
						<th scope="row">Restore Stock</th>
						<td>
							<label for="restore"><input name="restore" id="restore" value="1" type="checkbox" <?= ($restoreStock ? '' : 'checked') ?> > Restore Stock on cancelled orders?</label>
						</td>
					</tr>
					<? } ?>
				</table>
				<? submit_button(); ?>
			</form>
	<?
	}

	function logDisplay() {
		$log = get_option('scanventory_log');
		echo "<table class='log'>";
		echo "<thead>";
		echo "<th style='width:220px'>When</th><th style='width:220px'>Who</th><th>What</th>";
		echo "</thead>";
		echo "<tbody>";
		rsort($log);
		foreach($log as $event) {
			list($when,$who,$what) = $event;
			preg_match_all("/{[^}]*}/",$what,$matches);
			foreach($matches[0] as $match) {
				$pid = preg_replace('/\D/', '', $match);
				if ( function_exists( 'get_product' ) )
					$p = get_product( $pid );
				else
					$p = new WC_Product( $pid );

				$what = str_replace($match,"<A href='".get_edit_post_link( $pid, true )."'> {$p->post->post_title} </A>",$what);
			}

			$wdate = date("r",(int)$when);
			$cu = get_userdata($who);
			echo "<tr><td class='when'> {$wdate} </td><td class='who'><A HREF='".get_edit_user_link( $who )."'>{$cu->display_name}</A></td><td class='what'> {$what} </td></tr>";
		}
		echo "</tbody>";
		echo "</table>";
	}

	function labels() {
		?>
		<H2>Labels</H2>
			<form method="post" action="/?scanventory-labels" target="_BLANK">
				<table class="form-table">

					<tr valign="top">
						<th scope="row">Label Print Options</th>
						<td>
							<label for="allOption"><input name="labeloption" id="allOption" type="radio" <?= ($_REQUEST["target"] ? '' : 'checked') ?> > All Products</label>
						</td>
					</tr><tr valign="top">
						<th></th>
						<td>
							<?
								if ( ! isset($_REQUEST['target'])) {
									echo "<label for=\"singleOption\"><input disabled name=\"labeloption\" id=\"singleOption\" type=\"radio\"> Specific Products : </label>";
									echo "<I> No Products Specified </I> ";
								} else {
									echo "<label for=\"singleOption\"><input name=\"labeloption\" id=\"singleOption\" type=\"radio\" checked> Specific Products : </label>";
									$dp = explode(",",urldecode($_REQUEST['target']));
									 echo "<B>".count($dp)."</B> products specified";
									foreach($dp as $k=>$v) {
										echo "<input type=\"hidden\" name=\"products[]\" value=\"{$v}\">";
										$title = get_the_title($v);
										$titles[] = $title;
									}
									echo "<BR>&nbsp;&nbsp;";
									echo join(", ",$titles);
								}

							?>
						</td>
					</tr>


					<tr valign="top">
						<th scope="row">Additional Label Info </th>
						<td>
							<select name="variable">
								<?
									$vars = self::variables();

									foreach($vars as $k=>$v) {
										echo "<option value=\"{$k}\"> {$v["name"]}</option>";
									}

								?>
							</select></td>
					</tr>


					<tr valign="top">
						<th scope="row">Label Format</th>
						<td><select name="label">
								<?
									foreach(self::labelfac() as $css=>$label) {
										echo "<option value=\"{$css}\">{$label}</option>";
									}
								?>
							</select></td>
					</tr>


				</table>
				<?
					if (! $this->key->valid()) {
						echo "<H3>* Creating Temporary Labels <A TARGET=\"_BLANK\" HREF=\"http://www.scanventory.net/demo\">Read More</A></H3>";
					}
				?>
				<button class="button">Build Labels</button>
			</form>
			<BR>
		<?
	}

	function register() {
		self::gensecret();
		self::genlog();
		register_setting('scanventory_options', 'scanventory_secret');
	}

	function variables() {
		global $woocommerce;
		$vars = array(
			"*"=>array(
				'type'=>'all',
				'name'=>'All'
			),
			"none"=> array(
				'type'=>'none',
				'name'=>'None'
			),
			"price" => array(
				'type'=>'static',
				'name'=>'Price'
			)
		);
		//Gather variables for each possible product.
		$terms = $woocommerce->get_attribute_taxonomies();

		foreach($terms as $k=>$term) {
			$vars['a:pa_'.$term->attribute_name] = array(
				'name'=>'Attribute: '.$term->attribute_label,
				'type'=>'term',
				'data'=>$term
			);
		}

		return $vars;
	}

	function menu() {
		$scanpage = add_submenu_page( 'edit.php?post_type=product', 'Scanventory' , 'Scanventory', 'manage_woocommerce', 'scanventory', array($this,'main') );
	}

	static function links($links,$file) {
		if ( $file == 'scanventory/scanventory-admin.php' ) {
			$links['settings'] = sprintf( '<a href="%s"> %s </a>', admin_url( 'options-general.php?page=scanventory' ), __( 'Settings', 'scanventory' ) );
		}
		return $links;
	}

	function check() {
		global $post_type;
		if ( 'product' == $post_type ) {
			?>
			<script type="text/javascript">
				jQuery(document).ready(function() {
					jQuery('<option>').val('label').text('<?php _e( 'Build labels', 'woocommerce' )?>').appendTo("select[name='action']");
					jQuery('<option>').val('label').text('<?php _e( 'Build labels', 'woocommerce' )?>').appendTo("select[name='action2']");
/*
					jQuery('<option>').val('mark_completed').text('<?php _e( 'Mark completed', 'woocommerce' )?>').appendTo("select[name='action']");
					jQuery('<option>').val('mark_completed').text('<?php _e( 'Mark completed', 'woocommerce' )?>').appendTo("select[name='action2']");
*/
				});
			</script>
		<?php
		}

	}

	function custom() {
		if (($_REQUEST["post_type"] == 'product') and ($_REQUEST["action"] == 'label')) {
			$target = urlencode(join(",",$_REQUEST["post"]));
			header("Location: ?post_type=product&page=scanventory&target={$target}&tab=labels");
			die();
		}
	}
}

?>
