<?php
    class scanventory_mobile {

		private $key;
		private $id;

		static $instance;

		public static function instance( scanventory_license $kv = NULL ) {
			if ( ! isset( self::$instance )) {
				self::$instance = new scanventory_mobile( $kv );
			}
			return self::$instance;
		}

		public function __construct( scanventory_license $kv = NULL ) {
			if ( ! is_null($kv)) {
				$this->key = $kv;
			}
		}

		public function valid_request( $path=NULL ) {
			if (is_null($path)) return false;
			if (strstr($path,"&")) {
				$path = substr($path,0,strlen($path) - strlen(strstr($path,"&")));
			}
			$cur = 0;
			$buffer = '';
			for($i=13;$i<=strlen($path);$i++) {
				if ($path[$i] == 'g') {
					$cur = $buffer;
					$buffer = '';
				} else {
					$buffer .= $path[$i];
				}
			}

			$vo = $buffer;
			$id = hexdec($cur);

			$en = sha1($id + get_option('scanventory_secret'));
			if ($en === false) return false;
			if (strlen($en) < 4) return false;
			$ch = (substr( $en, (strlen($en) - 4) , 4 ));
			if ( strstr($vo,$ch) ) {
				$this->id = $id;
				return true;
			} else {
				return false;
			}
		}

	    private function checkAuth() {
			global $wpdb;
		    if (isset($_REQUEST["logout"])) {
			    wp_clear_auth_cookie();
			    echo "<H2>You have been logged out</H2>";
			    die();
		    }

		    if ((! current_user_can('edit_products')) or ( ! is_user_logged_in() )) {

			    if ($_SERVER["REQUEST_METHOD"] == "POST") {
				    $suser = $wpdb->escape($_POST["username"]);
				    $spass = $wpdb->escape($_POST["password"]);
				    wp_clear_auth_cookie();
				    $return = wp_authenticate($suser,$spass);
				    if (is_wp_error($return)) {
					    $ec = $return->get_error_code();
					    if ($ec == "empty_username") $message = "Empty Username";
					    if ($ec == "invalid_username") $message = "Incorrect Username";
					    if ($ec == "incorrect_password") $message = "Incorrect Password";
					    if ($ec == "empty_username") $message = "Account not found";
					    if ($ec == "empty_password") $message = "Empty Password";
				    } else {
					    $user_id = $return->ID;
					    wp_clear_auth_cookie();
					    wp_set_auth_cookie( $user_id, false );
					    wp_set_current_user( $user_id );
					    header("Location: ".$_SERVER["REQUEST_URI"]);
					    die();
				    }
			    }

			    if (is_user_logged_in())  $message = "You do not have rights to edit products";

			    $action = "login";
			    include_once( plugin_dir_path( __DIR__ ) . 'templates/mobile.php');
			    die();
		    }
	    }

        function post(WC_Product $p) {
            $message = '';
            if ($p === FALSE) {
                echo 'Unable to locate product.';
                die();
            }

            if (isset($_POST["main_set"])) {
                $p->set_stock( $_POST["main_stock"] );
                scanventory_log::instance("Updated {".$p->id."} stock to : {$_POST["main_stock"]}.");
                $message .= "Stock set to {$_POST["main_stock"]}\n<BR>";
            }

            if (isset($_POST["main_byone"])) {
                if ($_POST['main_byone'] == '-') {
                    $p->reduce_stock();
                    scanventory_log::instance("{".$p->id."} stock reduced by 1");
                    $message = "Stock reduced by 1";
                } elseif ($_POST['main_byone'] == '+') {
                    $p->increase_stock();
                    scanventory_log::instance("{".$p->id."} stock increased by 1");
                    $message = "Stock increased by 1";
                }
            }

            if (isset($_POST['backorders'])) {
				update_post_meta( $p->post->ID, '_backorders', $_POST['backorders']  );
			}
			if (isset($_POST['individually'])) {
				update_post_meta( $p->post->ID, '_sold_individually',  $_POST['individually']  );
			}


            if (isset($_POST['byone'])) {
                if (is_array($_POST['byone'])) {
                    foreach($_POST['byone'] as $aid=>$action) {
                        if ( function_exists( 'get_product' ) )
                            $ap = get_product( $aid );
                        else
                            $ap = new WC_Product( $aid );

                        if ($action == '-') {
                            $ap->reduce_stock();
	                        scanventory_log::instance("{".$aid."} stock increased by 1");
                            $message = "Stock #{$aid} reduced by 1";
                        } elseif ($action == '+') {
                            $ap->increase_stock();
	                        scanventory_log::instance("{".$aid."} stock increased by 1");
                            $message = "Stock #{$aid} increased by 1";
                        }
                    }
                }
            }

			if (isset($_POST["set"]) and (is_array($_POST["set"]))) {
				foreach($_POST["set"] as $vid=>$set) {
					if ( function_exists( 'get_product' ) )
						$ap = get_product( $vid );
					else
						$ap = new WC_Product( $vid );

					$ap->set_stock( $_POST["stock"][$vid] );
                    scanventory_log::instance("Updated {".$ap->id."} stock to : {$_POST["stock"][$vid]}.");
                    $message .= "#{$vid} Stock set to {$_POST["stock"][$vid]}\n<BR>";
				}
            }

            if ($_POST["cmd"] == "Activate") {
                update_post_meta( $p->id, '_manage_stock', true);
	            scanventory_log::instance("Activated {".$p->id."} inventory management.");
            } elseif ($_POST["cmd"] == "Deactivate") {
                update_post_meta( $p->id, '_manage_stock', false );
	            scanventory_log::instance("Deactivated {".$p->id."} inventory management.");
            } elseif ($_POST["cmd"] == "instock") {
                $p->set_stock_status('outofstock');
	            scanventory_log::instance("Stock Status for {".$p->id."} now 'Out of Stock'.");
            } elseif ($_POST["cmd"] == "outstock") {
                $g = $p->set_stock_status('instock');
	            scanventory_log::instance("Stack Status for {".$p->id."} now 'In Stock'.");
            }

            return $message;
        }

		function display( $id = NULL ) {
			global $wpdb,$woocommerce;

			if (is_null($id) and isset($this->id)) $id = $this->id;
            $message = "";
			$current= parse_url($_SERVER['REQUEST_URI']);
			$mobile_url = "/?".$current["query"];

			$this->checkAuth();

            if ( function_exists( 'get_product' ) )
                $p = get_product( $id );
            else
                $p = new WC_Product( $id );

            if ($p === FALSE) {
                echo 'Unable to locate product.';
                die();
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$message = $this->post($p);
                if ( function_exists( 'get_product' ) )
                    $p = get_product( $id );
                else
                    $p = new WC_Product( $id );
			}

			//Get Image
			$args = array(
				'post_type' => 'attachment',
				'numberposts' => -1,
				'post_parent' => $id,
				'orderby'=> 'name',
				'order'=>'ASC'
			);

			$thumbIDs = array();
			$attachments = get_posts( $args );
			if ( $attachments ) {
				foreach ( $attachments as $attachment ) {
					$thumbIDs[$attachment->post_name] = $attachment->ID;
				}
			} else {
				$thumbIDs = array();
			}

			//Load variations
			$variations = array($p->id=>$p);
			if ($p->product_type == 'variable') {
				$res = $wpdb->get_results("SELECT ID from {$wpdb->posts} p WHERE post_parent = {$p->id} and post_type = 'product_variation'");
				foreach($res as  $v) {
					if ( function_exists( 'get_product' ) )
						$variations[$v->ID] = get_product( $v->ID );
					else
						$variations[$v->ID] = new WC_Product( $v->ID );
				}
			} else {

			}

			if (count($thumbIDs) > 0) {
				$img_dat = wp_get_attachment_image_src( array_shift($thumbIDs)  );
				$image = "<img id=\"pi-{$p->id}\" src=\"".$img_dat[0]."\">";
			} else {
				$image = "<img id=\"pi-{$p->id}\" src=\"http://www.placehold.it/120x120&text=No+Image\">";
			}

			if ($p->managing_stock()) {
				$stock = $p->get_total_stock();
			} else {
				$stock = false;
			}

			$name = $p->post->post_title;
			$sku = $p->get_sku();
			$instock = ($p->is_in_stock() ? 'instock' : 'outofstock');
			$action = 'view';
			$backorders = $p->backorders;
			$individually = $p->sold_individually;

			//Load variation data
			$vars = array();
			if ($stock !== FALSE) {
				if (count($variations) > 0)	{
					foreach($variations as $variant) {
                        $var = array();
						$var["stock"] = $variant->get_stock_quantity();
						if ($variant->variation_has_sku) {
							$var["label"] = 'SKU: ' . $variant->get_sku();
						} else {
							$var["label"] = 'Variation #' . $variant->variation_id;
						}

						$var["vid"] = $variant->variation_id;
                        $var["has_stock"] = $variant->variation_has_stock;

						if (!$var["vid"]) {
							$var["label"] = 'All variants';
							continue;
						}

						if ($variant->variation_data != "") {
							$terms = $woocommerce->get_attribute_taxonomies();
							foreach ( $terms as $term) {
								$termMap['attribute_pa_'.$term->attribute_name] = $term->attribute_label;
							}
							foreach ($variant->variation_data as $attributeKey=>$value) {
								$attributes[] = array($termMap[$attributeKey],$value);
							}
							$var["attributes"] = $attributes;
							unset($attributes);
						}

						$vars[] = $var;
					}
				}
			}

			include_once( plugin_dir_path( __DIR__ ) . 'templates/mobile.php');

			die();
		}

    }
?>
